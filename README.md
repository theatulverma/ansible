# Ansible

**Running Playbooks:**
•	ansible-playbook: Run an Ansible playbook to execute a set of tasks on managed nodes.
ansible-playbook my_playbook.yml

**Ad Hoc Commands:**
•	ansible: Run ad hoc commands on managed nodes.
ansible my_hosts -m command -a "uptime"

1.**	Gathering Facts:**
•	ansible all -m setup: Collect information (facts) about managed nodes.

2.**	Inventory Management:**
•	ansible-inventory: Display the current inventory configuration.
•	ansible-inventory --list -y: List inventory details in YAML format.
•	-i: Specify a custom inventory file.

**3.	Running Tasks on a Single Host:**
•	ansible-playbook my_playbook.yml -l hostname: Run tasks only on a specific host.

4.	**Running Tasks with Tags:**
•	ansible-playbook my_playbook.yml --tags "tag_name": Run tasks with specific tags defined in the playbook.

5.**	Running Tasks with Limit:**
•	ansible-playbook my_playbook.yml --limit hostname: Run tasks only on specific hosts.
6.	Verbose Output:
•	-v, -vv, -vvv: Increase verbosity for debugging.

7**.	Checking Syntax:**
•	ansible-playbook my_playbook.yml --syntax-check: Check the syntax of a playbook.

8.	**Using Vault for Secrets:**
•	ansible-vault encrypt: Encrypt sensitive data in a file.
•	ansible-vault decrypt: Decrypt a vault-encrypted file.
•	ansible-playbook my_playbook.yml --ask-vault-pass: Prompt for the vault password during playbook execution.
9.	
**Creating and Managing SSH Keys:**
•	ssh-keygen: Generate SSH keys for use with Ansible.

10.	**Listing Modules and Documentation:**
•	ansible-doc: Display documentation for Ansible modules.

#ansible-doc command

**Gathering Facts: Collect information (facts) about managed nodes.**

ansible all -m setup
Listing Hosts in Inventory: Display the list of hosts defined in your inventory.
ansible-inventory –list

**Running Tasks on Specific Hosts:**
Run tasks only on specific hosts using patterns.
ansible-playbook my_playbook.yml --limit my_group

**Running Tasks with Tags: Run tasks with specific tags defined in the playbook.**
ansible-playbook my_playbook.yml --tags "tag_name"

**Checking Syntax of a Playbook: Check the syntax of a playbook without actually running it.**
ansible-playbook my_playbook.yml --syntax-check

**Using Vault for Encryption: Encrypt and decrypt sensitive data using Ansible Vault.**

ansible-vault encrypt my_file.yml
ansible-vault decrypt my_file.yml

**Debugging Tasks: Print debug messages during playbook execution.**
ansible-playbook my_playbook.yml –vvv

**Running Playbook with Prompt for Vault Password: Prompt for the Ansible Vault password during playbook execution.
ansible-playbook my_playbook.yml --ask-vault-pass

**Generating SSH Key Pair: Generate SSH keys for use with Ansible.**
ssh-keygen -t rsa -b 4096

**Displaying Documentation for a Module: Display documentation for Ansible modules.**
ansible-doc command
